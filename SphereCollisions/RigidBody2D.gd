extends RigidBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	set_linear_velocity(Vector2(1600,-80))
	set_bounce(1)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
